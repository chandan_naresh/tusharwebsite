import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landscape',
  templateUrl: './landscape.component.html',
  styleUrls: ['./landscape.component.scss']
})
export class LandscapeComponent implements OnInit {

images=[];
  constructor() { 
    
    for(var i=1;i<26;i++){
      this.images.push(`../../assets/imgs/landscape/${i}.jpeg`);
    }

  }

  ngOnInit() {
  }

}
