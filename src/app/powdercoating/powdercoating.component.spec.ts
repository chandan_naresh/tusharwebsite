import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowdercoatingComponent } from './powdercoating.component';

describe('PowdercoatingComponent', () => {
  let component: PowdercoatingComponent;
  let fixture: ComponentFixture<PowdercoatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowdercoatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowdercoatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
