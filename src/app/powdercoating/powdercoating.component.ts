import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-powdercoating',
  templateUrl: './powdercoating.component.html',
  styleUrls: ['./powdercoating.component.css']
})
export class PowdercoatingComponent implements OnInit {

  items=[];
  selectedVal:any={'name':'Polyster Coating'};
  constructor(http:HttpClient) {
    http.get('../assets/data/powdercoating/index.json').subscribe((r:any)=>{
      this.items=r;
      this.selectedVal=this.items[0];
    })
   }

  ngOnInit() {
  }

  selected(i:any){
    console.log(i)
    this.selectedVal=i;
  }
}
