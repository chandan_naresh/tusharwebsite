import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navpanel',
  templateUrl: './navpanel.component.html',
  styleUrls: ['./navpanel.component.css']
})
export class NavpanelComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  showLandscape(){
    console.log('landscape clicked')
    this.router.navigate(['/landscape']);
  }

  showPowderCoating(){
    this.router.navigate(['/powdercoating'])
  }

  showConsulting(){
    this.router.navigate(['/consulting'])
  }
}
