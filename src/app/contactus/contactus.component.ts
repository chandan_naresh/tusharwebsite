import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  frmGrp:FormGroup;
  constructor(fb:FormBuilder,private http:HttpClient) { 
    this.frmGrp=fb.group({
      'name':['',Validators.required],
      'email':['',Validators.required],
      'mobile':['',Validators.required],
      'message':[],
    })
  }

  ngOnInit() {
  }

  sendEmail(){
  
    if(!this.frmGrp.valid){
      return;
    }

    let headers : HttpHeaders=new HttpHeaders();
    //headers.append('Content-Type','application/x-www-form-urlencoded');
    //headers.append("Access-Control-Allow-Origin","true");
    headers.append('Content-Type', 'application/json');
  
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
    headers.append('Access-Control-Allow-Headers','Content-Type');
  
    
      let msg={
        'sender':this.frmGrp.get('email').value,
        'reciever':'sabeeshbalatvm@gmail.com',
        'subject':'New Email From Website',
        'body':`<h3>Sender Name : ${this.frmGrp.get('name').value}</h3>
        <h3>Sender Mobile: ${this.frmGrp.get('mobile').value}</h3>
        <h3>Sender Email: ${this.frmGrp.get('email').value}</h3>
        <div style="align-self:center">Message Content</div>
        <p style="align-items:center">${this.frmGrp.get('message').value}</p>`
      }
      
      this.http.post('http://156.67.216.39:3002/sendEmail',msg,{headers:headers}).subscribe(r=>{
        console.log('email sent successfully');
        //this.successMessage="Thanks for contacting us. We will revert back within 24 hours";
        this.frmGrp.reset();
      },err=>{
        //this.errorMessage="Oops... there was an error encountered while sending email. please try again";
        console.log(err);
      })
  
  
  
  }


}
