import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {NgxPageScrollModule} from 'ngx-page-scroll';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavpanelComponent } from './navpanel/navpanel.component';
import { FooterComponent } from './footer/footer.component';
import { LandscapeComponent } from './landscape/landscape.component';
import { PowdercoatingComponent } from './powdercoating/powdercoating.component';
import { ConsultingComponent } from './consulting/consulting.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { ExportComponent } from './export/export.component';


const routes:Routes=[
{path:'',pathMatch:'full', redirectTo:'home'},
{path:'home',component:HomeComponent},
{path:'aboutus',component:AboutusComponent},
{path:'landscape',component:LandscapeComponent},
{path:'powdercoating',component:PowdercoatingComponent},
{path:'consulting',component:ConsultingComponent},
{path:'contactus',component:ContactusComponent},

]
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavpanelComponent,
    FooterComponent,
    LandscapeComponent,
    PowdercoatingComponent,
    ConsultingComponent,
    ToolbarComponent,
    AboutusComponent,
    ContactusComponent,
    ExportComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgxPageScrollModule,    
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
