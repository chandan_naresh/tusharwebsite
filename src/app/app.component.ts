import { Component } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Mrunmayi Agro';
  bgcolor:any="#1d795ba6";
  device:boolean;
  constructor(private router:Router){

    console.log(window.screen.width);
    if(window.screen.width<400){
      this.device=true;
    }else{
      this.device=false;
    }
    this.router.events.subscribe((event:any)=>{
      if (event instanceof NavigationEnd ) {
        console.log(event.url); 
      if(event.url == "/home" || event.url=="/"){
        //this.bgcolor="#1d795ba6";
        this.bgcolor="white";
        //this.bgcolor="transparent";
      }else{
        this.bgcolor=" rgb(38, 228, 38)";
      }
      }
    })


  }

}


